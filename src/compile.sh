#!/usr/bin/env bash

file="main"
root="$( dirname -- "$( readlink -f -- "${0}"; )"; )/.."
bin="${root}/bin"

mkdir -p "${bin}" && cd "${bin}"

as -o "${file}.o" "${root}/src/${file}.asm"
ld -o "${file}.out" "${file}.o"
shred -u -- "${file}.o"
strip "${file}.out"
chmod +x "${file}.out"

echo "Compiled binary: ./$( basename -- "${bin}" ; )/${file}.out"
exit 0
