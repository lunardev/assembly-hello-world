.section .text
.globl _start

_start:
    mov     $1,     %eax     # Use the `sys_write` system call.
    mov     $msg,   %esi     # Use ascii string `msg` as message.
    mov     $len,   %edx     # Write 14 characters.
    mov     %eax,   %edi     # Write to stdout.
    syscall                  # Make the system call.
    mov     $60,    %eax     # Use the `sys_exit` system call.
    xor     %edi,   %edi     # Exit code 0.
    syscall                  # Make the system call.

msg:
    .ascii "Hello, world!\n" # ASCII hello world string.
    len = . - msg            # Get the address length.
