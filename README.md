# "Hello World" in Assembly Language

### Code (AT&T syntax):
```nasm
.section .text
.globl _start

_start:
    mov     $1,     %eax     # Use the `sys_write` system call.
    mov     $msg,   %esi     # Use ascii string `msg` as message.
    mov     $len,   %edx     # Write 14 characters.
    mov     %eax,   %edi     # Write to stdout.
    syscall                  # Make the system call.
    mov     $60,    %eax     # Use the `sys_exit` system call.
    xor     %edi,   %edi     # Exit code 0.
    syscall                  # Make the system call.

msg:
    .ascii "Hello, world!\n" # ASCII hello world string.
    len = . - msg            # Get the address length.
```

### Compile:
```bash
$ as -o main.o main.asm
$ ld -o main.out main.o && shred -u main.o
$ strip main.out
```

### Run:
```bash
$ chmod +x main.out
$ ./main.out
Hello, world!
```

### Automatic script:
```bash
$ ./src/compile.sh
Compiled binary: ./bin/main.out
$ ./bin/main.out
Hello, world!
```

This project is licensed under the [MIT license](./LICENSE).
